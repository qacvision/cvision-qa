package com.xbsoftware.cvision.base;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Context {

    private static Logger LOG = Logger.getLogger(Context.class);
    private static Context context;
    private static WebDriver webDriver;
    //todo add property to maven
    private static final String BROWSER_CHROME = "chrome";


    public Context() {
    }

    public static WebDriver getDriver() {
        return webDriver;
    }

    public static void initDriver() {
        webDriver = new FirefoxDriver();
       // webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
    }

    public static void closeDriver() {
        webDriver.close();
    }
}
