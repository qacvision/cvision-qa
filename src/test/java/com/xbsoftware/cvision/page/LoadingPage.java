package com.xbsoftware.cvision.page;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class LoadingPage extends LoginPage {

    public static String LOADING_PAGE_URL = "http://cvisionqa.tk/";
    private WebDriver driver;

    public static LoadingPage openLoadingPage() {
        LoginPage loginPage = LoginPage.openLoginPage();
        LoadingPage loadingPage = loginPage.login();
        return loadingPage;
    }

    public static LoadingPage openLandingPageFreeUser() {
        LoginPage loginPage = LoginPage.openLoginPage();
        LoadingPage loadingPage = loginPage.loginFreeUser();
        return loadingPage;
    }

    public void setFavorite() {
        findElementClick(By.cssSelector(".nav-tabs .insight-group-item:nth-child(1)"));
    }

    public void setMyInsights() {
        findElementClick(By.cssSelector(".nav-tabs .insight-group-item:nth-child(2)"));
    }

    public void setSharedWithMe() {
        findElementClick(By.cssSelector(".nav-tabs .insight-group-item:nth-child(3)"));
    }

    public void setRecent() {
        findElementClick(By.cssSelector(".nav-tabs .insight-group-item:nth-child(4)"));
    }

    public void setAllAvailable() {
        findElementClick(By.cssSelector(".nav-tabs .insight-group-item:nth-child(5)"));
    }

    public String getTitleTabs() {
        return getElement(By.cssSelector(".active .insight-group-title"));
    }

    public void setViewTable() {
        findElementClick(By.cssSelector(".fa-align-justify"));
    }

    public String getTitleOfTable() {
        return getElement(By.cssSelector(".insight-title"));
    }

   public String getMessageEmptyMyInsight() {
        return getElement(By.cssSelector(".insights-group-empty-information-description"));
    }

    public void setlinkCreateNewInsight() {
        findElementClick(By.cssSelector(".insights-group-empty-information-create-insight-button"));
    }

    public String getMessageEmptyGroupInsights() {
        return getElement(By.cssSelector(".insights-group-empty-information-title"));
    }

    public void setPreviewInsight(){findElementClick(By.cssSelector(".insight-icon-item-preview"));}

    public void setPanelInsight(){moveCursorToElement(By.cssSelector(".insight-icon-item-title"));}

    public void setPreviewInsigthPage(){
        setPanelInsight();
        setPreviewInsight();
    }

    public void setFavoriteIcon(){findElementClick(By.cssSelector(".fa-star-o"));}

    public String getTitleInsight(){return getElement(By.cssSelector(".insight-icon-item-title"));}

    public void setCopyIcon(){findElementClick(By.cssSelector(".copy-insight"));}

    public String getTitleInsightOnDataPage(){return getElement(By.cssSelector(".insight-page-top-menu-insight-title"));}

    //delete insight

    public void setDeleteIcon(){findElementClick(By.cssSelector(".icon-trash"));}

    //public String getTitleConfirmationDeleteInsight(){return getElement(By.cssSelector(".in[style='display: block;'] .modal-title b"));}

    public void openDeleteConfirmation(){
        setView();
        setDeleteIcon();
    }

    public void setOkButtonDeleteInsight(){findElementClick(By.cssSelector(".in[style='display: block;'] button[item_id='button_813']"));}

    public void setCancelButtonDeleteInsight(){findElementClick(By.cssSelector(".in[style='display: block;'] button[item_id='button_108']"));}

    public void setCloseButtonDeleteInsight(){findElementClick(By.cssSelector(".in[style='display: block;'] .sphere-modal-close"));}

    //share insight

    public void setShareIcon(){findElementClick(By.cssSelector(".icon-share-alt:nth-child(2)"));}

    public String getTitlePopup(){return getElement(By.cssSelector(".in[style='display: block;'] .modal-title b"));}

   /* public void setAnyUserOfCVision(){
       // findElementClick(By.cssSelector(".share-type-permissions"));
        ((JavascriptExecutor)driver).executeScript("document.querySelectorAll(\".share-type-permissions button\")[1].click()");
         }
*/
    public void openShareInsight(){
        setView();
        setShareIcon();
    }
}
