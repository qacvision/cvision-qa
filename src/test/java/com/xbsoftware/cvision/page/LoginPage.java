package com.xbsoftware.cvision.page;

import org.openqa.selenium.By;

public class LoginPage extends Page{

    private static String LOGIN_PAGE_URL = "http://cvisionqa.tk/login";

    public static LoginPage openLoginPage(){
        LoginPage loginPage = new LoginPage();
        loginPage.getDriver().get(LOGIN_PAGE_URL);
        return loginPage;
    }

    public void login(String user, String password){
        setUserName(user);
        setPassword(password);
        pushLoginButton();
    }

    public LoadingPage login(){
        login("test1","1");
        return new LoadingPage();
    }

    public LoadingPage loginFreeUser(){
        login("salisandra90@mail.ru","11111111");
        return new LoadingPage();
    }

    public void failsLoginAs(){
        login("test1","123");
    }

    public void setUserName(String userName) {
        inputField(By.cssSelector("input[name='username']"), userName);
    }

    public void setPassword(String password) {
        inputField(By.cssSelector("input[name='password']"), password);
    }

    public void pushLoginButton() {
        findElementClick(By.cssSelector(".pull-right"));
    }

    public String getErrorMessage() {
        return getElement(By.cssSelector(".display-hide span"));
    }

    public String getWelcomeMessage() {
        return getElement(By.cssSelector(".uppercase"));
    }

    public String getErrorEmail(){
        return getElement(By.cssSelector("span[for='username']"));
    }

    public String getErrorPassword(){
        return getElement(By.cssSelector("span[for='password']"));
    }

    public void checkRememberMe(){ checkedElements(By.cssSelector(".checkbox")); }

    public void uncheckRememberMe(){ uncheckedElements(By.cssSelector(".checkbox"));
    }

    public void resetPage() { findElementClick(By.id("forget-password"));
    }

    public String getMessageResetPage(){
        return getElement(By.cssSelector(".reset-form h3"));
    }

    public void submitResetPassword() { findElementClick(By.cssSelector(".reset-form .form-actions>button"));}

    public String getErrorMessageEmailResetPassword(){ return getElement(By.cssSelector(".help-block"));}

    public void setEmailResestPassword(String email){inputField(By.cssSelector("input[name='email']"), email);}

    public void setBackResetPassword(){ findElementClick(By.cssSelector(".form-actions>a"));}

    public String getMessageLoginPage(){ return getElement(By.cssSelector(".form-title"));}

    public void createAccount(){
        login();
        findElementClick(By.id("register-btn"));
    }

    public String random(){
        Integer length = 8;
        String name = randomText(length);
        return name;
    }

    public void randomNotCorrectEmail(){
        setEmailResestPassword(random());
    }

    public void randomEmail(){
        setEmailResestPassword(randomEmailAddres());
    }

}
