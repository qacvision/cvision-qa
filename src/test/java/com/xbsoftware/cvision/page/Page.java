package com.xbsoftware.cvision.page;


import com.xbsoftware.cvision.base.Context;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public abstract class Page {
    private static Logger LOG = Logger.getLogger(Page.class);
    private WebDriver driver;

    public Page() {
        driver = Context.getDriver();
    }

    protected WebDriver getDriver() {
        return driver;
    }

    public void setView(){
        waitForElement(By.cssSelector(".insight-icon-item-title"));
        ((JavascriptExecutor)driver).executeScript("document.querySelector(\".insight-icon-item:nth-child(3) .insight-icon-item-details\").style.top = \"0px\"");
    }

    public void setAnyUserOfCVision(){
        // findElementClick(By.cssSelector(".share-type-permissions"));
        ((JavascriptExecutor)driver).executeScript("document.querySelectorAll(\".share-type-permissions button\")[0].click()");
    }

    public String randomEmailAddres(){
        Date curTime = new Date();
        DateFormat date= DateFormat.getDateInstance();
        String dateTime = date.format(curTime) + "_" + System.currentTimeMillis();
        String email = "email_" + dateTime + "@test.test" ;
        return email;
     }

    protected void inputField(By element, String msg) {
        WebElement input = driver.findElement(element);
        input.clear();
        input.sendKeys(msg);
    }

    public static String ramdomNumber() {
        Integer max = 23000;
        Integer number1 = (int) (Math.random() * (max - 1) + 1);
        String number = number1.toString();
        return number;
    }

    public static String randomText(int lengthOfText) {
        String string = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        String[] abc = string.split(",");
        Integer max = abc.length;
        String text = "";
        for (int i = 0; i < lengthOfText; i++) {
            Integer number = (int) (Math.random() * (max - 1) + 1);
            text += abc[number];
        }
        return text;
    }

    public void checkedElements(By findElement) {
        WebElement element = driver.findElement(findElement);
        if (element.getAttribute("checked") != null) {
            String str = "ok";
        } else {
            String className = getClass().toString();
            LOG.error("Element '" + findElement + "' unchecked in class '" + className + "' method '" + getMethodName(className) + "'");
        }
    }

    public void uncheckedElements(By findElement) {
        WebElement element = driver.findElement(findElement);
        if (element.getAttribute("checked") == null) {
            String str = "ok";
        } else {
            String className = getClass().toString();
            LOG.error("Element '" + findElement + "' checked in class '" + className + "' method '" + getMethodName(className) + "'");

        }
    }

    public void assertElements(By findElement) {
        try {
            WebElement element = driver.findElement(findElement);


        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
        }
    }

    // Method to find elements and click
    public void findElementClick(By findElement)  {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement element = wait.until(visibilityOfElementLocated(findElement));
            element = driver.findElement(findElement);
            element.click();

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'" );
        }
    }

    public static String getMethodName(String className) {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

        for (int i = 0; i < ste.length; i++) {
            System.out.println(ste[i].getClassName() + ", " + className + ", " + ste[i].toString());
            if (className.contains(ste[i].getClassName())) {
                return ste[i].getMethodName();
            }
        }
        return "Unknown";
    }
    public void timeout() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void selectOption(String value) {
        try {
            List<WebElement> allSelects = driver.findElements(By.tagName("select"));
            for (WebElement select : allSelects) {
                List<WebElement> allOptions = select.findElements(By.tagName("option"));
                for (WebElement option : allOptions) {
                    if (option.getAttribute("value").contains(value)) {
                        option.click();
                        return;
                    }
                }
            }
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found select option '" + value + "' in class '" + className + "' method '" + getMethodName(className) + "'");
        }
    }

    protected ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement toReturn = driver.findElement(locator);
                if (toReturn.isDisplayed()) {
                    return toReturn;
                }
                return null;
            }
        };
    }

    public void waitForElement(By element) {
        Wait<WebDriver> wait = new WebDriverWait(driver, 40);
        WebElement elementWait = wait.until(visibilityOfElementLocated(element));
    }

    public void checkToError(String error, String textError) {
        if (!error.equals(textError)) {
            String className = getClass().toString();
            LOG.error("Error not correct...  in class '" + className + "' method '" + getMethodName(className) + "'");
        }
    }

    public void moveCursorToElement(By element) {
        waitForElement(element);
        new Actions(driver).moveToElement(driver.findElement(element)).perform();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void dndElements(By dragElement, By targetElement) {
        try {
            WebElement draggable = driver.findElement(dragElement);
            String drag = draggable.toString();
            WebElement target = driver.findElement(targetElement);
            new Actions(driver).clickAndHold(draggable).moveToElement(target).release().perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Can not dnd elements in class " + className + "' method '" + getMethodName(className) + "'");
        }
    }

    protected String getElement(By by){
        waitForElement(by);
        return driver.findElement(by).getText();
    }
}
