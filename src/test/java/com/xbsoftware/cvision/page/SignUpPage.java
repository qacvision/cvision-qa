package com.xbsoftware.cvision.page;


import org.openqa.selenium.By;

import java.text.DateFormat;
import java.text.Format;
import java.util.Date;

public class SignUpPage extends LoginPage{

    private static String SIGNUP_PAGE_URL = "http://cvisionqa.tk/signup";

    public static SignUpPage openSignUpPage(){
        SignUpPage singupPage = new SignUpPage();
        singupPage.getDriver().get(SIGNUP_PAGE_URL);
        return singupPage;
    }

    public void signUp(){
        checkAgree();
        signUpPage();
    }

    public void signUpPage(){
        findElementClick(By.id("register-btn"));
    }

    public void setFirstName(String firstName){
        inputField(By.cssSelector("input[name='first_name']"), firstName);
    }

    public void setLastName(String lastName){
        inputField(By.cssSelector("input[name='last_name']"), lastName);
    }

    public void setEmail(String email){
        inputField(By.cssSelector("input[name='email']"), email);
    }

    public void pushSignUp(){
        findElementClick(By.id("register-submit-btn"));
    }

    public String getErrorFirstName(){
        return getElement(By.cssSelector("span[for='first_name']"));
    }

    public String getErrorLastName(){
        return getElement(By.cssSelector("span[for='last_name']"));
    }

    public String getErrorEmail(){
        return getElement(By.cssSelector("span[for='email']"));
    }

    public String getTextPage(){
        return getElement(By.cssSelector(".sign-up-form p"));
    }

    public void checkAgree(){
        findElementClick(By.cssSelector("input[name='tnc']"));
    }

    public void setLoginPage(){
        findElementClick(By.linkText("log in"));
    }

    public String getMessageLoginPage(){
        return getElement(By.cssSelector(".form-title"));
    }

    public String random(){
        Integer length = 8;
        String name = randomText(length);
        return name;
    }

    public void randomFirstName(){
        setFirstName(random());
    }

    public void randomLastName(){
        setLastName(random());
    }

    public void randomNotCorrectEmail(){
        setEmail(random());
    }

    public void randomEmail(){
        setEmail(randomEmailAddres());
    }


}
