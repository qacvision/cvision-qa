package com.xbsoftware.cvision.testpage;


import com.xbsoftware.cvision.base.Context;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

public class BaseTest extends TestCase{

    private static Logger LOG = Logger.getLogger(BaseTest.class);

    @BeforeMethod
    public void beforeMethod(Method method) {
        LOG.info("# "+ method.getName() + "start");
        Context.initDriver();
    }

    @AfterMethod
    public void afterMethod(Method method) {
        LOG.info("### "+ method.getName() + "end");
        Context.closeDriver();
    }
}
