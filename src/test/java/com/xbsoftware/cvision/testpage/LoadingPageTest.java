package com.xbsoftware.cvision.testpage;


import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.xbsoftware.cvision.page.LoadingPage;
import com.xbsoftware.cvision.page.LoginPage;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class LoadingPageTest extends BaseTest{



    @Test
    public void openLoadinPage(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        assertTrue(loadingPage.getWelcomeMessage().contains("INSIGHT SPHERE"));
    }

    // Groups insights

    @Test
    public void openTabFavorite(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setFavorite();
        assertTrue(loadingPage.getTitleTabs().contains("Favorite"));
    }

    @Test
    public void emptyFavorite(){
        LoadingPage loadingPage = LoadingPage.openLandingPageFreeUser();
        loadingPage.setFavorite();
        assertTrue(loadingPage.getMessageEmptyGroupInsights().contains("No favorite insights"));
    }

    @Test
    public void openTabMyInsights(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        assertTrue(loadingPage.getTitleTabs().contains("My Insights"));
    }

    @Test
    public void emptyMyInsights(){
        LoadingPage loadingPage = LoadingPage.openLandingPageFreeUser();
        assertTrue(loadingPage.getMessageEmptyMyInsight().contains("CVision lets you explore large databases as compact devices and access them on every computer and mobile devices"));
    }

    @Test void createNewInsightInTabMAyInsight(){
        LoadingPage loadingPage = LoadingPage.openLandingPageFreeUser();
        loadingPage.setlinkCreateNewInsight();
        //not assert
    }

    @Test
    public void openTabSharedWithMe(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setSharedWithMe();
        assertTrue(loadingPage.getTitleTabs().contains("Shared with me"));
    }

    @Test
    public void emptySharedWithMe(){
        LoadingPage loadingPage = LoadingPage.openLandingPageFreeUser();
        loadingPage.setSharedWithMe();
        assertTrue(loadingPage.getMessageEmptyGroupInsights().contains("Insights that other have shared with you"));
    }

    @Test
    public void openTabRecent(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setRecent();
        assertTrue(loadingPage.getTitleTabs().contains("Recent"));
    }

    @Test
    public void emptyRecent(){
        LoadingPage loadingPage = LoadingPage.openLandingPageFreeUser();
        loadingPage.setRecent();
        assertTrue(loadingPage.getMessageEmptyGroupInsights().contains("Insights that you work with last time"));
    }

    @Test
    public void openTabAllAvailable(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setAllAvailable();
        assertTrue(loadingPage.getTitleTabs().contains("All available"));
    }

    @Test
    public void emptyAllAvailable(){
        LoadingPage loadingPage = LoadingPage.openLandingPageFreeUser();
        loadingPage.setAllAvailable();
        assertTrue(loadingPage.getMessageEmptyGroupInsights().contains("All the insights you have an access to"));
    }

     @Test
    public void viewTable(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setViewTable();
        assertTrue(loadingPage.getTitleOfTable().contains("title"));
    }

    //Insight grid(view)


    @Test
    public void openPanelInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setPanelInsight();
        //нет проверки
    }

    @Test
    public void makeInsightFavorite(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setView();
        String title  = loadingPage.getTitleInsight();
        System.out.println(title);
        loadingPage.setFavoriteIcon();
        loadingPage.setFavorite();
        loadingPage.timeout();
        assertTrue(loadingPage.getTitleInsight().contains(title));
    }

    @Test
    public void copyInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setView();
        String name = loadingPage.getTitleInsight() + "_copy";
        loadingPage.setCopyIcon();
        assertTrue(loadingPage.getTitleInsightOnDataPage().contains(name));
    }

    @Test
    public void deleteInsightOpenPopup(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.openDeleteConfirmation();
        assertTrue(loadingPage.getTitlePopup().contains("Confirmation"));
    }

    @Test
    public void deleteInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.openDeleteConfirmation();
        loadingPage.setOkButtonDeleteInsight();
        //как проверить что элемиент удален ?
    }

    @Test
    public void deleteInsightCancel(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        String name = loadingPage.getTitleInsight();
        loadingPage.openDeleteConfirmation();
        loadingPage.setCancelButtonDeleteInsight();
        assertTrue(loadingPage.getTitleInsight().contains(name));
    }

    @Test
    public void deleteInsightCloseConfirmation(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        String name = loadingPage.getTitleInsight();
        loadingPage.openDeleteConfirmation();
        loadingPage.setCloseButtonDeleteInsight();
        assertTrue(loadingPage.getTitleInsight().contains(name));
    }

    //share insight

    @Test
    public void shareInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setView();
        loadingPage.setShareIcon();
        assertTrue(loadingPage.getTitlePopup().contains("Share insight"));
    }

    @Test
    public void shareInsightChangeAnyUserOfInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.openShareInsight();

        loadingPage.setAnyUserOfCVision();
    }


    @Test
    public void previewInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setView();
        loadingPage.setPreviewInsight();
        //проверка
    }

    @Test
    public void moveToInsight(){
        LoadingPage loadingPage = LoadingPage.openLoadingPage();
        loadingPage.setPreviewInsigthPage();

    }





}
