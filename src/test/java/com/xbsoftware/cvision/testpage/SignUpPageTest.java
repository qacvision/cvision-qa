package com.xbsoftware.cvision.testpage;

import com.xbsoftware.cvision.page.LoginPage;
import com.xbsoftware.cvision.page.SignUpPage;
import org.apache.log4j.Logger;
import org.mvel2.ast.Sign;
import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SignUpPageTest extends BaseTest {

    private static Logger LOG = Logger.getLogger(TestLoginPage.class);

    @Test
    public void signUpPage(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.pushSignUp();
        assertTrue(signUp.getTextPage().contains("Enter your personal details below:"));
    }

    @Test
    public void emptyFirstName(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.pushSignUp();
        assertTrue(signUp.getErrorFirstName().contains("First name is required."));
    }

    @Test
    public void emptyLastName(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.pushSignUp();
        assertTrue(signUp.getErrorLastName().contains("Last name is required."));
    }

    @Test
    public void emptyEmail(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.pushSignUp();
        assertTrue(signUp.getErrorEmail().contains("Email is required."));
    }

    @Test
    public void notCorrectEmail(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.randomFirstName();
        signUp.randomLastName();
        signUp.randomNotCorrectEmail();
        signUp.pushSignUp();
        assertTrue(signUp.getErrorEmail().contains("Email should be valid"));
    }

    @Test
    public void signUp(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.randomFirstName();
        signUp.randomLastName();
        signUp.randomEmail();
        signUp.checkAgree();
        signUp.pushSignUp();
        signUp.timeout();
        assertTrue(signUp.getErrorMessage().contains("Confirmations sent"));
    }

    @Test
    public void checkAgree(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.checkAgree();
        signUp.assertElements(By.cssSelector("input[name='tnc'][aria-invalid='false']"));
    }

    @Test
    public void uncheckAgree(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.checkAgree();
        signUp.checkAgree();
        signUp.assertElements(By.cssSelector("input[name='tnc'][aria-invalid='true']"));
    }

    @Test
    public void loginPage(){
        SignUpPage signUp = SignUpPage.openSignUpPage();
        signUp.setLoginPage();
        assertTrue(signUp.getMessageLoginPage().contains("Login to your account"));
    }



}
