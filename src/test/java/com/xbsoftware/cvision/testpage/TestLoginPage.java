package com.xbsoftware.cvision.testpage;


import com.xbsoftware.cvision.page.LoginPage;
import com.xbsoftware.cvision.page.LoadingPage;
import com.xbsoftware.cvision.page.Page;
import org.apache.log4j.Logger;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class TestLoginPage extends BaseTest {

    private static Logger LOG = Logger.getLogger(TestLoginPage.class);


    // log in

    @Test
    public void loginTestPage(){
        LoginPage loginPage = LoginPage.openLoginPage();
        LoadingPage loadingPage = loginPage.login();
        assertTrue(loadingPage.getWelcomeMessage().contains("INSIGHT SPHERE"));
    }

    @Test
    public void loginEmptyEmail(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.pushLoginButton();
        assertTrue(loginPage.getErrorEmail().contains("Email is required."));
    }

    @Test
    public void loginEmptyPassword(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.setUserName("test");
        loginPage.pushLoginButton();
        assertTrue(loginPage.getErrorPassword().contains("Password is required."));
    }

    @Test
    public void loginNotRegistedUser(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.login("123", "1");
        assertTrue(loginPage.getErrorMessage().contains("Username/Password do not match"));
    }

    @Test
    public void loginNotCorrectPassword(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.failsLoginAs();
        assertTrue(loginPage.getErrorMessage().contains("Username/Password do not match"));
    }

    @Test
    public void rememberMeChecked(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.checkRememberMe();
        loginPage.assertElements(By.cssSelector(".checkbox .checked"));
        loginPage.uncheckRememberMe();
    }

    @Test
    public void rememberMeUnchecked(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.checkRememberMe();
        loginPage.checkRememberMe();
        loginPage.assertElements(By.cssSelector(".checkbox .checker"));
    }

    @Test
    public void errorMessageClose(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.failsLoginAs();
        loginPage.pushLoginButton();
        loginPage.timeout();
        loginPage.findElementClick(By.cssSelector("div[style='display: block;'] .close"));
        loginPage.assertElements(By.cssSelector(".alert-danger[style='display: none;']"));
    }


    // Reset password

    @Test
    public void resetPage(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.resetPage();
        assertTrue(loginPage.getMessageResetPage().contains("Forget Password ?"));
    }

    @Test
    public void submitEmptyEmailResetPassword(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.resetPage();
        loginPage.submitResetPassword();
        assertTrue(loginPage.getErrorMessageEmailResetPassword().contains("Email is required"));
    }

    @Test
    public void submitNotCorrectEmailResetPassword(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.resetPage();
        loginPage.randomNotCorrectEmail();
        assertTrue(loginPage.getErrorMessageEmailResetPassword().contains("Email is incorrect"));
    }

    @Test
    public void submitEmailResetPassword(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.resetPage();
        loginPage.randomEmail();
        //assertTrue(loginPage); не работает, дописать
    }

    @Test
    public void backResetPassword(){
        LoginPage loginPage = LoginPage.openLoginPage();
        loginPage.resetPage();
        loginPage.setBackResetPassword();
        assertTrue(loginPage.getMessageLoginPage().contains("Login to your account"));
    }
}
