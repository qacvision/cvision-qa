package com.xbsoftware.cvision.util;


import com.xbsoftware.cvision.base.Context;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

public class TestUtil {
    public static void takeScreenShot(String fileName) {
        File srcFile = ((TakesScreenshot)(Context.getDriver())).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, new File("/screenshots/" + fileName + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
